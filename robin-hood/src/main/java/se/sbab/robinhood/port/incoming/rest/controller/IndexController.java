package se.sbab.robinhood.port.incoming.rest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import se.sbab.robinhood.port.outgoing.rest.FriarTuckAdapterImpl;

@RestController
@RequestMapping("/api/v1")
@Api(value = "/api/v1", description = "API version v1")
public class IndexController {

  @ApiOperation(value = "Are You Alive?", response = String.class)
  @RequestMapping(value = "/alive", method = RequestMethod.GET)
  public String areYouAlive() {
    return "Robin Hood Still Alive";
  }

  @ApiOperation(value = "Kill")
  @RequestMapping(value = "/kill", method = RequestMethod.GET)
  public void kill() {
    System.exit(-1);
  }

}
