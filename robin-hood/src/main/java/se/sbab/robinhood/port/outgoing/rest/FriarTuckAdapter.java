package se.sbab.robinhood.port.outgoing.rest;

public interface FriarTuckAdapter {
  int checkFriarTuck();
}
