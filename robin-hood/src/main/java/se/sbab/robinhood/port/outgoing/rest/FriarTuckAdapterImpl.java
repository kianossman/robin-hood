package se.sbab.robinhood.port.outgoing.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class FriarTuckAdapterImpl implements FriarTuckAdapter {

  @Override
  public int checkFriarTuck() {
    RestTemplate template = new RestTemplate();
    String url = "http://localhost:8092/friar-tuck/api/v1/alive";

    try {
      ResponseEntity<String> response = template.getForEntity(url, String.class);
      if (response.getStatusCode() != HttpStatus.OK) {
        return 0;
      }
    } catch(Exception e) {
      System.out.println(e);
      return 0;
    }
    return 1;
  }
}
