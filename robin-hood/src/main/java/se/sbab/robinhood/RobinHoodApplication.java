package se.sbab.robinhood;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import se.sbab.robinhood.port.outgoing.rest.FriarTuckAdapterImpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

@SpringBootApplication
public class RobinHoodApplication {

	public static void main(String[] args) {
		SpringApplication.run(RobinHoodApplication.class, args);

		FriarTuckAdapterImpl friarTuck = new FriarTuckAdapterImpl();
		Timer timer = new Timer();


		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (friarTuck.checkFriarTuck() == 0) {

					String s = null;

					try{
						Process runtime = Runtime.getRuntime().exec("java -jar ../friar-tuck/target/friar-tuck-0.0.1-SNAPSHOT.jar");

						BufferedReader stdError = new BufferedReader(new
								InputStreamReader(runtime.getErrorStream()));

						BufferedReader stdInput = new BufferedReader(new
								InputStreamReader(runtime.getInputStream()));


						while ((s = stdInput.readLine()) != null) {
							System.out.println(s);
						}

						while ((s = stdError.readLine()) != null) {
							System.out.println(s);
						}

					}catch(Exception e){
						System.out.println(e);
					}
					System.out.println("Starta om Friar Tuck");
				};
			}
		}, 0, 10000);
	}
}
