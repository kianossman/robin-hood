package se.sbab.friartuck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import se.sbab.friartuck.port.outgoing.rest.RobinHoodAdapterImpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

@SpringBootApplication
public class FriarTuckApplication {

	public static void main(String[] args) {
		SpringApplication.run(FriarTuckApplication.class, args);

		RobinHoodAdapterImpl robinHood = new RobinHoodAdapterImpl();
		Timer timer = new Timer();

		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (robinHood.checkRobinHood() == 0) {

					String s = null;

					try{
						//System.setProperty("user.dir", "/Users/rala/Documents/innovationday/robin-hood/friar-tuck");

						Process runtime = Runtime.getRuntime().exec("java -jar ../robin-hood/target/robin-hood-0.0.1-SNAPSHOT.jar");

						BufferedReader stdError = new BufferedReader(new
								InputStreamReader(runtime.getErrorStream()));

						BufferedReader stdInput = new BufferedReader(new
								InputStreamReader(runtime.getInputStream()));

						while ((s = stdInput.readLine()) != null) {
							System.out.println(s);
						}

						while ((s = stdError.readLine()) != null) {
							System.out.println(s);
						}

					}catch(Exception e){
						System.out.println(e);
					}
					System.out.println("Starta om Robin Hood");
				};
			}
		}, 0, 10000);


	}
}
