package se.sbab.friartuck.port.outgoing.rest;

public interface RobinHoodAdapter {
  int checkRobinHood();
}
